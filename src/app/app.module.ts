import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routing } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { PaisesComponent } from './paises/paises.component';
import { ErrorComponent } from './error/error.component';
import { PaisComponent } from './pais/pais.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    PaisesComponent,
    ErrorComponent,
    PaisComponent
  ],
  imports: [
    BrowserModule,
    Routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
