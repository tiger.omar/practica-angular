import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { PaisesComponent } from './paises/paises.component';
import { ErrorComponent } from './error/error.component';

export const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'paises', component: PaisesComponent },
  { path: '**', component: ErrorComponent }
];

export const Routing = RouterModule.forRoot(routes,
  {} // { enableTracing: true }
);

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
