import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Pais } from './domain/pais';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {
  urlBase = 'https://restcountries-v1.p.rapidapi.com/';

  cache: Pais[] = null;

  constructor(private http: HttpClient) { }


  getAllmock(): Pais[]{
    return [
      {
        name: 'bolivia',
        capital: 'Sucre'
      },
      {
        name: 'España',
        capital: 'Madrid'
      }
    ];
  }

  getAll(): Observable<Pais[]> {
    if (this.cache) {
      return of(this.cache);
    }

    const options = {
      headers: {
        'Content-Type': 'application/json',
        'x-rapidapi-host': 'restcountries-v1.p.rapidapi.com',
        'x-rapidapi-key': '868c7aac99msh251b116ac237236p1e8edfjsnca552019f3e0'
      }
    };

    return this.http.get(this.urlBase + 'all', options).pipe(
      map(d => {
        const paises = toPaises(d);
        this.cache = paises;
        return paises;
      })
    );
  }
}

function toPaises(array: any): Pais[] {
  return array as Pais[];
}
