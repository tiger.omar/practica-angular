import { Component, OnInit, OnDestroy } from '@angular/core';
import { Pais } from '../domain/pais';
import { Subscription } from 'rxjs';
import { GetDataService } from '../get-data.service';

@Component({
  selector: 'app-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss']
})
export class PaisesComponent implements OnInit, OnDestroy {
  paises: Pais[] = null;
  error: string = null;

  private sub: Subscription;

  constructor(private getDataService: GetDataService) { }

  ngOnInit() {
    this.paises = this.getDataService.getAllmock();
    // this.sub = this.getDataService.getAll().subscribe(
    //   data => { this.loadData(data); },
    //   error => { this.handleError(error); }
    // );
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
      this.sub = null;
    }
  }

  loadData(data: Pais[]): void {
    this.error = null;
    this.paises = data;
  }
  handleError(error: any): void {
    this.error = error.message;
    this.paises = [];
  }
}
